import React from "react";

import copyCodeBlock from "@pickra/copy-code-block";

import "highlight.js";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import ListGroup from "react-bootstrap/ListGroup";
import Accordion from "react-bootstrap/Accordion";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import "../../assets/fonts.scss";
import "../../assets/glyphs.scss";

import "../styles/homepage.scss";

export default {
  title: "Developers",
  parameters: {
    options: { showPanel: false }
  }
};

function copyCodeBlock1() {
  return { __html: copyCodeBlock("npm install") };
}

export const Overview = () => <div></div>;

export const Getting_Started = () => <div></div>;

class Link extends React.Component {
  render() {
    return (
      <a className={this.props.className} href={this.props.to}>
        {this.props.children}
      </a>
    );
  }
}

Overview.story = {
  parameters: {
    paddings: { disabled: false }
  }
};
