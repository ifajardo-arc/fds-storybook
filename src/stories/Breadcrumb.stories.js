import React from "react";
import { action } from "@storybook/addon-actions";
import Breadcrumb from "react-bootstrap/Breadcrumb";

export default {
  title: "Breadcrumb",
  component: Breadcrumb
};

export const Overview = () => (
  <div class="container">
    <h2 className="type-heading-h2">Breadcrumb</h2>
    <p>
      The breadcrumb within Flight DS is a secondary navigation component that
      displays the content hierarchy while tracing the user’s path. It allows
      for quick navigation to a parent level or previous step. All links in a
      breadcrumb should be clickable with the exception of the current page
      label. Breadcrumbs should always be treated as secondary navigation and
      should never be used in place of primary navigation. Its condensed design
      makes this component a great choice for organizing large amounts of
      content while visually showing heirarchy.
    </p>

    <h3 className="type-heading-h3-secondary">Formatting & Content</h3>
    <ul>
      <li>Do not use icons within text links.</li>
      <li>Strive to keep link names to three words or less.</li>
      <li>
        Use meaningful, descriptive link names that describe the intended
        destination.
      </li>
      <li>
        Display no more than three links within a breadcrumb trail. (use the
        overflow menu if more are required)
      </li>
    </ul>
  </div>
);

export const Basic = () => (
  <div class="container">
    <div className="row">
      <div className="col-md-12">
        <Breadcrumb>
          <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="/#/bootstrap">Introduction</Breadcrumb.Item>
          <Breadcrumb.Item active>Breadcrumb</Breadcrumb.Item>
        </Breadcrumb>
      </div>
    </div>
  </div>
);
