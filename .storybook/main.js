const path = require("path");

module.exports = {
  stories: ["../src/**/*.stories.js"],
  addons: [
    "@storybook/addon-docs",
    "@storybook/preset-create-react-app",
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-storysource",
    "storybook-addon-paddings",
    "@storybook/addon-viewport/register"
  ],
  webpackFinal: async (config, { configType }) => {
    // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
    // You can change the configuration based on that.
    // 'PRODUCTION' is used when building the static version of storybook.

    // Make whatever fine-grained changes you need
    config.module.rules.push({
      test: /\.(sa|sc|c)ss$/,
      use: ["style-loader", "css-loader", "sass-loader"],
      include: path.resolve(__dirname, "../")
    });

    config.module.rules.push({
      test: /\.(woff|woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
      use: ["file-loader"],
      include: path.resolve(__dirname, "../")
    });

    config.resolve.symlinks = false;

    // Return the altered config
    return config;
  }
};
