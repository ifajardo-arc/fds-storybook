import React from "react";
import { action } from "@storybook/addon-actions";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";

export default {
  title: "Form Components",
  component: Form
};

export const Overview = () => (
  <div className="container">
    <div className="row">
      <div className="col-md-12">
        <h1 className="type-heading-h1">Form Components</h1>
        <p>
          Cards are a highly flexible component for displaying a wide variety of
          content. They can range from basic, text-only cards to complex cards
          containing many other Flight components. The card itself is nothing
          more than a rectangle or square that responds to the grid. The only
          pre-set styles are the elevation and radius tokens used. Beyond that,
          you can customize the cards content and design to meet your specific
          needs. Just be sure to leverage the other Flight design tokens for
          typography, space and color.
        </p>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Text Input</h2>
        <p>
          The Text Input component gives users the ability to enter and edit
          text. It is used for long form and short form content. Use the text
          input component when you expect the user to input a single line entry
          and the text area component for entries longer than one sentence.
        </p>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Select</h2>
        <p>
          The select component gives users the ability to submit data within
          forms by choosing one option from a list.In the future, more types may
          be needed as components are added to Flight. (e.g. Small and Inline
          selects)
        </p>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Checkboxes and Radios</h2>
        <p>
          For the non-textual checkbox and radio controls, FormCheck provides a
          single component for both types that adds some additional styling and
          improved layout.
        </p>
      </div>
    </div>
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Text Input</h2>
        <p>
          The Text Input component gives users the ability to enter and edit
          text. It is used for long form and short form content. Use the text
          input component when you expect the user to input a single line entry
          and the text area component for entries longer than one sentence.
        </p>
      </div>
    </div>
  </div>
);

export const Text_Input = () => (
  <div style={{ maxWidth: "400px" }}>
    <form>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
    </form>
  </div>
);

export const Select = () => (
  <div style={{ maxWidth: "400px" }}>
    <form>
      <Form.Group controlId="exampleForm.ControlSelect2">
        <Form.Label>Example select</Form.Label>
        <Form.Control as="select">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Form.Control>
      </Form.Group>
    </form>
  </div>
);

export const Checkbox = () => (
  <Form>
    {["checkbox"].map(type => (
      <div key={`default-${type}`} className="mb-3">
        <Form.Check
          type={type}
          id={`default-${type}`}
          label={`default ${type}`}
        />

        <Form.Check
          type={type}
          id={`default-${type}`}
          label={`default ${type}`}
        />

        <Form.Check
          disabled
          type={type}
          label={`disabled ${type}`}
          id={`disabled-default-${type}`}
        />
      </div>
    ))}
  </Form>
);

export const Radio = () => (
  <Form>
    {["radio"].map(type => (
      <div key={`default-${type}`} className="mb-3">
        <Form.Check
          type={type}
          id={`default-${type}`}
          label={`default ${type}`}
          name="radio"
        />

        <Form.Check
          type={type}
          id={`default-${type}-2`}
          label={`default ${type} 2`}
          name="radio"
        />

        <Form.Check
          disabled
          type={type}
          label={`disabled ${type}`}
          id={`disabled-default-${type}`}
          name="radio"
        />
      </div>
    ))}
  </Form>
);

export const Text_Area = () => (
  <form style={{ maxWidth: "400px" }}>
    <Form.Group controlId="exampleForm.ControlTextarea1">
      <Form.Label>Example textarea</Form.Label>
      <Form.Control as="textarea" rows="3" />
    </Form.Group>
  </form>
);
