import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";

export default {
  title: "Dropdown",
  component: Dropdown
};

export const Primary = () => (
  <Dropdown className="fds-dropdown">
    <Dropdown.Toggle id="dropdown-basic">
      Dropdown Button
    </Dropdown.Toggle>
  
    <Dropdown.Menu>
      <Dropdown.Item href="#/components/dropdown">Action</Dropdown.Item>
      <Dropdown.Item href="#/components/dropdown">
        Another action
      </Dropdown.Item>
      <Dropdown.Item href="#/components/dropdown">
        Something else
      </Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

export const Split_Button = () => (
  <Dropdown as={ButtonGroup}>
    <Button variant="success">Split Button</Button>

    <Dropdown.Toggle split variant="success" id="dropdown-split-basic" />

    <Dropdown.Menu>
      <Dropdown.Item >Action</Dropdown.Item>
      <Dropdown.Item >Another action</Dropdown.Item>
      <Dropdown.Item >Something else</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);
