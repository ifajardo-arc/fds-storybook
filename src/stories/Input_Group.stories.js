import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import FormControl from "react-bootstrap/FormControl";

export default {
  title: "Input Group",
  component: Dropdown
};

export const Primary = () => (
  <div>
    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl
        placeholder="Username"
        aria-label="Username"
        aria-describedby="basic-addon1"
      />
    </InputGroup>

    <InputGroup className="mb-3">
      <FormControl
        placeholder="Recipient's username"
        aria-label="Recipient's username"
        aria-describedby="basic-addon2"
      />
      <InputGroup.Append>
        <InputGroup.Text id="basic-addon2">@example.com</InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>

    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text id="basic-addon3">
          https://example.com/users/
        </InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl id="basic-url" aria-describedby="basic-addon3" />
    </InputGroup>

    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text>$</InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl aria-label="Amount (to the nearest dollar)" />
      <InputGroup.Append>
        <InputGroup.Text>.00</InputGroup.Text>
      </InputGroup.Append>
    </InputGroup>

    <InputGroup>
      <InputGroup.Prepend>
        <InputGroup.Text>With textarea</InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl as="textarea" aria-label="With textarea" />
    </InputGroup>
  </div>
);

export const Sizing = () => (
  <div>
    <InputGroup size="sm" className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text id="inputGroup-sizing-sm">Small</InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" />
    </InputGroup>
    <br />
    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <InputGroup.Text id="inputGroup-sizing-default">
          Default
        </InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl
        aria-label="Default"
        aria-describedby="inputGroup-sizing-default"
      />
    </InputGroup>
    <br />
    <InputGroup size="lg">
      <InputGroup.Prepend>
        <InputGroup.Text id="inputGroup-sizing-lg">Large</InputGroup.Text>
      </InputGroup.Prepend>
      <FormControl aria-label="Large" aria-describedby="inputGroup-sizing-sm" />
    </InputGroup>
  </div>
);

export const Button_Addons = () => (
  <div style={{ maxWidth: "600px" }}>
    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <Button variant="primary">Button</Button>
      </InputGroup.Prepend>
      <FormControl aria-describedby="basic-addon1" />
    </InputGroup>

    <InputGroup className="mb-3">
      <FormControl
        placeholder="Recipient's username"
        aria-label="Recipient's username"
        aria-describedby="basic-addon2"
      />
      <InputGroup.Append>
        <Button variant="primary">Button</Button>
      </InputGroup.Append>
    </InputGroup>

    <InputGroup className="mb-3">
      <InputGroup.Prepend>
        <Button variant="primary">Button</Button>
        <Button variant="secondary">Button</Button>
      </InputGroup.Prepend>
      <FormControl aria-describedby="basic-addon1" />
    </InputGroup>

    <InputGroup>
      <FormControl
        placeholder="Recipient's username"
        aria-label="Recipient's username"
        aria-describedby="basic-addon2"
      />
      <InputGroup.Append>
        <Button variant="secondary">Button</Button>
        <Button variant="primary">Button</Button>
      </InputGroup.Append>
    </InputGroup>
  </div>
);

export const Dropdown_Addon = () => (
  <div className="pt-3">
    <InputGroup className="mb-3">
      <DropdownButton
        as={InputGroup.Prepend}
        title="Dropdown"
        id="input-group-dropdown-1"
        variant="secondary"
      >
        <Dropdown.Item href="#">Action</Dropdown.Item>
        <Dropdown.Item href="#">Another action</Dropdown.Item>
        <Dropdown.Item href="#">Something else here</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item href="#">Separated link</Dropdown.Item>
      </DropdownButton>
      <FormControl aria-describedby="basic-addon1" />
    </InputGroup>

    <InputGroup>
      <FormControl
        placeholder="Recipient's username"
        aria-label="Recipient's username"
        aria-describedby="basic-addon2"
      />

      <DropdownButton
        as={InputGroup.Append}
        title="Dropdown"
        id="input-group-dropdown-2"
        variant="secondary"
      >
        <Dropdown.Item href="#">Action</Dropdown.Item>
        <Dropdown.Item href="#">Another action</Dropdown.Item>
        <Dropdown.Item href="#">Something else here</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item href="#">Separated link</Dropdown.Item>
      </DropdownButton>
    </InputGroup>
  </div>
);
