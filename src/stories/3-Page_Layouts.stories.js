import React from "react";

import copyCodeBlock from "@pickra/copy-code-block";

import "highlight.js/styles/tomorrow-night.css";

import Button from "react-bootstrap/Button";
import Jumbotron from "react-bootstrap/Jumbotron";
import NavDropdown from "react-bootstrap/NavDropdown";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import "../styles/page-layout.scss";
import "../../node_modules/@arccommon/flight-dev-kit/dist/fds-bootstrap/fds-bootstrap.min.css";

export default {
  title: "Page Layouts",
  parameters: {
    options: { showPanel: false }
  }
};

class ExamplePageHeader extends React.Component {
  render() {
    return (
      <Navbar expand="sm">
        <Container>
          <Navbar.Brand href="#home">
            <img
              src="https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-l-teal.png"
              width="65"
              height="24"
              className="d-inline-block align-baseline"
              alt="Airlines Reporting Corporation Logo"
            />
            Product Name
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#link">Link</Nav.Link>
              <Nav.Link href="#link">Link</Nav.Link>
              <NavDropdown
                className="fds-dropdown"
                title="Dropdown Link"
                id="basic-nav-dropdown"
              >
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Another action
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Something
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <div className="navbar-utility">
              <a href="" className="navbar-utility-link navbar-utility-icon">
                <i className="fds-button__icon fds-glyphs-bell2"></i>
              </a>
              <a
                href="#"
                className="navbar-utility-link navbar-utility-username"
              >
                User Name
              </a>
              <a href="#" className="navbar-utility-link">
                Help
              </a>
              <a href="#" className="navbar-utility-link">
                Close
              </a>
            </div>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}

class ExamplePageFooter extends React.Component {
  render() {
    return (
      <div className="footer">
        <div className="container">
          <div className="row">
            <div className="col-lg-7">
              <p>&copy; 2019 Airlines Reporting Corporation</p>
              <p>
                SENSITIVE AND CONFIDENTIAL. Data presented or downloaded here
                may contain ARC-sensitive information. Disclosure to 3rd parties
                is prohibited without prior written consent of ARC.
              </p>
            </div>
            <div className="col-lg-5">
              <div className="footer-links">
                <a className="fds-link" href="#">
                  Terms Of Use
                </a>
                <a className="fds-link" href="#">
                  Privacy Policy
                </a>
                <a className="fds-link" href="#">
                  Contact Us
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export const Overview = () => (
  <Container>
    <Row>
      <Col>
        <h1 className="type-heading-h1 mb-5">Page Layouts</h1>
      </Col>
    </Row>
    <Row>
      <Col>
        <h2 className="type-heading-h2">Level 0</h2>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt
          optio officia nemo ex! Dicta doloribus quas cupiditate aspernatur
          fugit tempore praesentium perspiciatis accusantium nostrum eos alias
          consectetur, sunt, quo delectus?
        </p>
      </Col>
      <Col>
        <h2 className="type-heading-h2">Level 1</h2>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt
          optio officia nemo ex! Dicta doloribus quas cupiditate aspernatur
          fugit tempore praesentium perspiciatis accusantium nostrum eos alias
          consectetur, sunt, quo delectus?
        </p>
      </Col>
      <Col>
        <h2 className="type-heading-h2">Level 2</h2>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt
          optio officia nemo ex! Dicta doloribus quas cupiditate aspernatur
          fugit tempore praesentium perspiciatis accusantium nostrum eos alias
          consectetur, sunt, quo delectus?
        </p>
      </Col>
      <Col>
        <h2 className="type-heading-h2">Level 3</h2>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt
          optio officia nemo ex! Dicta doloribus quas cupiditate aspernatur
          fugit tempore praesentium perspiciatis accusantium nostrum eos alias
          consectetur, sunt, quo delectus?
        </p>
      </Col>
    </Row>
  </Container>
);

export const Structure = () => (
  <Container>
    <Row>
      <Col>
        <h1 className="type-heading-h1">Structure</h1>
        <h2 className="type-heading-h2 mb-5">
          Page layout patterns utilize a combination of built-in Bootstrap
          components and FDS modifier classes.
        </h2>
      </Col>
    </Row>
    <Row>
      <Col>
        <div>
          <div
            className="copyCodeBlock"
            dangerouslySetInnerHTML={pagelayoutCodeBlock()}
          />
        </div>
      </Col>
    </Row>
  </Container>
);

export const Level_One = () => (
  <div className="page">
    <ExamplePageHeader></ExamplePageHeader>

    <div className="fds-pl level-one">
      <Jumbotron className="fds-pl-jumbotron fds-pl-primary text-center" fluid>
        <Container>
          <h1 className="type-heading-h1-on-dark">
            Main Page Title Center Aligned
          </h1>
          <h2 className="type-heading-h2-subtitle-on-dark">
            Supporting Title Subtext
          </h2>
        </Container>
      </Jumbotron>

      <Container className="fds-pl-container">
        <Row>
          <Col lg="12">
            <div className="fds-pl-column shadow"></div>
          </Col>
        </Row>
      </Container>
    </div>

    <ExamplePageFooter></ExamplePageFooter>
  </div>
);

export const Level_One_Two_Column = () => (
  <div className="page">
    <ExamplePageHeader></ExamplePageHeader>

    <div className="fds-pl level-one">
      <Jumbotron className="fds-pl-jumbotron fds-pl-primary text-center" fluid>
        <Container>
          <h1 className="type-heading-h1-on-dark">
            Main Page Title Center Aligned
          </h1>
          <h2 className="type-heading-h2-subtitle-on-dark">
            Supporting Title Subtext
          </h2>
        </Container>
      </Jumbotron>

      <Container className="fds-pl-container">
        <Row>
          <Col lg="4">
            <div className="fds-pl-column shadow"></div>
          </Col>
          <Col lg="8">
            <div className="fds-pl-column shadow"></div>
          </Col>
        </Row>
      </Container>
    </div>

    <ExamplePageFooter></ExamplePageFooter>
  </div>
);

export const Level_Two = () => (
  <div className="page">
    <ExamplePageHeader></ExamplePageHeader>

    <div className="fds-pl level-two">
      <Jumbotron className="fds-pl-jumbotron fds-pl-primary text-left" fluid>
        <Container>
          <h1 className="type-heading-h1-on-dark">
            Main Page Title Left Aligned
          </h1>
          <h2 className="type-heading-h2-subtitle-on-dark">
            Supporting Title Subtext
          </h2>
        </Container>
      </Jumbotron>

      <Container className="fds-pl-container">
        <Row>
          <Col lg="12">
            <div className="fds-pl-column shadow"></div>
          </Col>
        </Row>
      </Container>
    </div>

    <ExamplePageFooter></ExamplePageFooter>
  </div>
);

export const Level_Two_Two_Column = () => (
  <div className="page">
    <ExamplePageHeader></ExamplePageHeader>

    <div className="fds-pl level-two">
      <Jumbotron className="fds-pl-jumbotron fds-pl-primary text-left" fluid>
        <Container>
          <h1 className="type-heading-h1-on-dark">
            Main Page Title Left Aligned
          </h1>
          <h2 className="type-heading-h2-subtitle-on-dark">
            Supporting Title Subtext
          </h2>
        </Container>
      </Jumbotron>

      <Container className="fds-pl-container">
        <Row>
          <Col lg="4">
            <div className="fds-pl-column shadow"></div>
          </Col>
          <Col lg="8">
            <div className="fds-pl-column shadow"></div>
          </Col>
        </Row>
      </Container>
    </div>

    <ExamplePageFooter></ExamplePageFooter>
  </div>
);

export const Level_Three = () => (
  <div className="page">
    <ExamplePageHeader></ExamplePageHeader>

    <div className="fds-pl level-three">
      <Jumbotron className="fds-pl-jumbotron fds-pl-white text-left" fluid>
        <Container>
          <h1 className="type-heading-h1">Main Page Title Left Aligned</h1>
          <h2 className="type-heading-h2-subtitle">Supporting Title Subtext</h2>
        </Container>
      </Jumbotron>

      <Container className="fds-pl-container">
        <Row>
          <Col lg="12">
            <div className="fds-pl-column shadow"></div>
          </Col>
        </Row>
      </Container>
    </div>

    <ExamplePageFooter></ExamplePageFooter>
  </div>
);

function pagelayoutCodeBlock() {
  return {
    __html: copyCodeBlock(
      `<div className="fds-pl level-one">
  <Jumbotron className="fds-pl-jumbotron text-center" fluid>
    <Container>
      <h1 className="type-heading-h1-on-dark">
        Main Page Title Center Aligned
      </h1>
      <h2 className="type-heading-h2-subtitle">
        Supporting Title Subtext
      </h2>
    </Container>
  </Jumbotron>

  <Container className="fds-pl-container shadow">
    <Row>
      <Col>Your Content</Col>
    </Row>
    </Container>
</div>`,
      {
        cssOverrides: `
  .container {
    background: #1b1b1b;
    padding: 1rem;
  }
`,
        lang: "html"
      }
    )
  };
}

Level_One.story = {
  parameters: {
    paddings: { disabled: true },
    options: { showPanel: true }
  }
};

Level_One_Two_Column.story = {
  parameters: {
    paddings: { disabled: true },
    options: { showPanel: true }
  }
};

Level_Two.story = {
  parameters: {
    paddings: { disabled: true },
    options: { showPanel: true }
  }
};

Level_Two_Two_Column.story = {
  parameters: {
    paddings: { disabled: true },
    options: { showPanel: true }
  }
};

Level_Three.story = {
  parameters: {
    paddings: { disabled: true },
    options: { showPanel: true }
  }
};
