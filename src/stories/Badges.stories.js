import React from "react";
import { action } from "@storybook/addon-actions";
import Badge from "react-bootstrap/Badge";

export default {
  title: "Badge",
  component: Badge
};

export const Overview = () => (
  <div className="container">
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Badge</h2>
        <p>
          The badge component is used to call out a numerical value. They can be
          paied with a FDS glyph or used inline. There are three badge types
          defined: alert, information, affirmation.
        </p>
      </div>
    </div>
    <div className="row">
      <div className="col-md-4">
        <h3 className="type-heading-h3-secondary">Red</h3>
        <Badge pill variant="danger">
          5
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="danger">
          55
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="danger">
          555
        </Badge>
      </div>
      <div className="col-md-4">
        <h3 className="type-heading-h3-secondary">Green</h3>
        <Badge pill variant="success">
          5
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="success">
          55
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="success">
          555
        </Badge>
      </div>
      <div className="col-md-4">
        <h3 className="type-heading-h3-secondary">Teal</h3>
        <Badge pill variant="primary">
          5
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="primary">
          55
        </Badge>{" "}
        &nbsp;
        <Badge pill variant="primary">
          555
        </Badge>
      </div>
    </div>
  </div>
);

export const Red = () => (
  <div>
    <Badge pill variant="danger">
      5
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="danger">
      55
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="danger">
      555
    </Badge>
  </div>
);

export const Green = () => (
  <div>
    <Badge pill variant="success">
      5
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="success">
      55
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="success">
      555
    </Badge>
  </div>
);

export const Teal = () => (
  <div>
    <Badge pill variant="primary">
      5
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="primary">
      55
    </Badge>{" "}
    &nbsp;
    <Badge pill variant="primary">
      555
    </Badge>
  </div>
);
