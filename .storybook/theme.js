import { create } from '@storybook/theming/create';

// https://storybook.js.org/docs/configurations/theming/

export default create({
  base: 'light',

  colorSecondary: '#59CADD',

  appBg: '#F8F8F8',
  appBorderColor: '#EDEDED',
  appBorderRadius: 6,

  barTextColor: '#999999',
  barSelectedColor: '#59CADD',
  barBg: '#F2F2F2',

  inputBg: 'white',
  inputBorder: 'rgba(0,0,0,.1)',
  inputTextColor: '#333333',
  inputBorderRadius: 4,

  brandImage: 'https://www2.arccorp.com/globalassets/arc-logos/corporate-logos/arc-logo-s-teal.png',
  brandUrl: 'https://www2.arccorp.com',
});