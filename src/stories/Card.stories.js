import React from "react";
import { action } from "@storybook/addon-actions";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";

export default {
  title: "Card",
  component: Card
};

export const Overview = () => (
  <div className="container">
    <div className="row">
      <div className="col-md-12">
        <h2 className="type-heading-h2">Card</h2>
        <p>
          Cards are a highly flexible component for displaying a wide variety of
          content. They can range from basic, text-only cards to complex cards
          containing many other Flight components. The card itself is nothing
          more than a rectangle or square that responds to the grid. The only
          pre-set styles are the elevation and radius tokens used. Beyond that,
          you can customize the cards content and design to meet your specific
          needs. Just be sure to leverage the other Flight design tokens for
          typography, space and color.
        </p>
      </div>
    </div>
  </div>
);

export const Basic = () => (
  <div style={{ maxWidth: "400px" }}>
    <Card>
      <Card.Body>
        <Card.Title as="h1" className="type-heading-h1">
          Card Title
        </Card.Title>
        <Card.Text className="type-body-primary-on-light">
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="primary">Click Here</Button>
      </Card.Body>
    </Card>
  </div>
);