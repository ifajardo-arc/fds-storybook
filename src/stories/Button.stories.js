import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Dropdown from "react-bootstrap/Dropdown";

export default {
  title: "Button",
  component: Button
};

export const Primary = () => <Button variant="primary">Primary Button</Button>;

export const Primary_Disabled = () => (
  <Button variant="primary" disabled onClick={action("clicked")}>
    Primary Button
  </Button>
);

export const Primary_Icon = () => (
  <Button variant="primary">
    Primary Button &nbsp;
    <i className="fds-glyphs fds-glyphs-plus"></i>
  </Button>
);

export const Primary_Icon_Disabled = () => (
  <Button variant="primary" disabled>
    Primary Button &nbsp;
    <i className="fds-glyphs fds-glyphs-plus"></i>
  </Button>
);

export const Split_Button = () => (
  <Dropdown as={ButtonGroup}>
    <Button variant="success">Split Button</Button>

    <Dropdown.Toggle split variant="success" id="dropdown-split-basic" />

    <Dropdown.Menu>
      <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
      <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
      <Dropdown.Item href="#/action-3">Something else</Dropdown.Item>
    </Dropdown.Menu>
  </Dropdown>
);

export const Variants = () => (
  <div>
    <Button variant="primary" onClick={action("clicked")}>
      Primary Button
    </Button>

    &nbsp;

    <Button variant="success" onClick={action("clicked")}>
      Affirmation Button
    </Button>

    &nbsp;

    <Button variant="danger" onClick={action("clicked")}>
      Destructive Button
    </Button>

    &nbsp;

    <Button variant="secondary" onClick={action("clicked")}>
      Secondary Button
    </Button>

    &nbsp;

    <Button variant="link" onClick={action("clicked")}>
      Ghost Button
    </Button>
  </div>
);
