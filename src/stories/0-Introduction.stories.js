import React from "react";

import copyCodeBlock from "@pickra/copy-code-block";

import "highlight.js";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import CardDeck from "react-bootstrap/CardDeck";
import ListGroup from "react-bootstrap/ListGroup";
import Accordion from "react-bootstrap/Accordion";
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import "../../assets/fonts.scss";
import "../../assets/glyphs.scss";
 
import "../../node_modules/@arccommon/flight-dev-kit/dist/fds-bootstrap/fds-bootstrap.min.css";

import "../styles/main.scss";
import "../styles/homepage.scss";

export default {
  title: "Introduction",
  parameters: {
    options: { showPanel: false }
  }
};

function copyCodeBlock1() {
  return { __html: copyCodeBlock("npm install") };
}

export const Overview = () => (
  <div className="homepage-wrapper">
    <div className="homepage-nav">
      <div className="homepage-nav">
        <div className="container">
          <div className="homepage-nav-title">
            Flight <span>Design System</span>
          </div>
          <Link to="/designers/flight-design-kit" className="homepage-nav-link">
            Designers
          </Link>
          <Link to="/developers/product-teams" className="homepage-nav-link">
            Developers
          </Link>
          <Link to="/design-tokens/overview" className="homepage-nav-link">
            Design Tokens
          </Link>
          <Link
            to="/component-blueprints/overview"
            className="homepage-nav-link"
          >
            Component Blueprints
          </Link>
        </div>
      </div>
    </div>
    <div className="homepage-jumbo">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md-8">
            <h1 className="type-heading-h1-xl-on-dark">
              <span>Flight</span> Design System
            </h1>
            <h2 className="type-heading-h2-on-dark">
              The Flight Design System is ARC's living source of truth to guide
              and empower designers and product teams to quickly build
              consistent product experiences at scale. The FDS will continue to
              mature as transformation products are designed. The system
              consists of working code, design guidelines, tools, and resources.
            </h2>
            <Link to="/?path=/story/introduction--getting-started">
              <Button variant="primary">Get Started</Button>
            </Link>{" "}
            &nbsp;
            <Link to="/component-blueprints/overview">
              <Button variant="secondary">View Components</Button>
            </Link>
          </div>
          <div className="col-md-4">
            <ListGroup>
              <ListGroup.Item className="startDesigning">
                <Link to="/?path=/story/designers--overview">
                  <div className="row align-items-center">
                    <div className="col-md-4">
                      <img
                        src="https://www2.arccorp.com/globalassets/fds/fds-design-icon.png"
                        alt=""
                      />
                    </div>
                    <div className="col-md-8">
                      <h3 className="type-heading-h3-primary-on-dark">
                        Start Designing
                      </h3>
                    </div>
                  </div>
                </Link>
              </ListGroup.Item>
              <ListGroup.Item className="startDeveloping">
                <Link to="/?path=/story/developers--overview">
                  <div className="row align-items-center">
                    <div className="col-md-4">
                      <img
                        src="https://www2.arccorp.com/globalassets/fds/fds-developer-icon.png"
                        alt=""
                      />
                    </div>
                    <div className="col-md-8">
                      <h3 className="type-heading-h3-primary-on-dark">
                        Start Developing
                      </h3>
                    </div>
                  </div>
                </Link>
              </ListGroup.Item>
            </ListGroup>
          </div>
        </div>
      </div>
    </div>

    <div className="homepage-cards">
      <div className="container">
        <CardDeck>
          <Card>
            <Card.Body>
              <Card.Title as="h1" className="type-heading-h1">
                Flight Components
              </Card.Title>
              <Card.Text className="type-body-primary-on-light">
                Component Blueprints demonstrate the intended visual style and
                interactive behavior for Flight UI components. They are written
                in HTML, CSS and React for teams translating UI components into
                functional, framework-specific code.
              </Card.Text>
              <Link to="/component-blueprints/overview">
                <Button variant="primary">Learn More</Button>
              </Link>
            </Card.Body>
          </Card>

          <Card>
            <Card.Body>
              <Card.Title as="h1" className="type-heading-h1">
                Design Tokens
              </Card.Title>
              <Card.Text className="type-body-primary-on-light">
                Design tokens store the platform-agnostic, visual design
                attributes that make up the design system. They are valuable
                because they can be easily updated in one central location and
                translated to any platform.
              </Card.Text>
              <Link to="/design-tokens/overview">
                <Button variant="primary">Learn More</Button>
              </Link>
            </Card.Body>
          </Card>

          <Card>
            <Card.Body>
              <Card.Title as="h1" className="type-heading-h1">
                Wondering how to contribute?
              </Card.Title>
              <Card.Text className="type-body-primary-on-light">
                See why a Design System is key to the integreity of our
                customers experience, and how you can be a part of it. Checkout
                the guidelines to get started.
              </Card.Text>
              <Link to="/contributions/overview">
                <Button variant="primary">Learn More</Button>
              </Link>
            </Card.Body>
          </Card>
        </CardDeck>
      </div>
    </div>

    <div className="homepage-styleguide">
      <div className="container">
        <div className="row">
          <div className="col-md-9">
            <h2 className="type-heading-h2-xl mb-3">Who is Flight for?</h2>

            <div className="row">
              <div className="col-md-6">
                <h3 className="type-heading-h3">Designers</h3>
                <h3 className="type-heading-h3-secondary mb-3">
                  Flight’s design kit, guidelines, and documentation help
                  designers work faster and smarter. It is home to an
                  ever-growing repo of tools, resources, and best practices that
                  help designers make consistent human-centered ARC product
                  experiences.
                </h3>
                <Link to="/designers/flight-design-kit">
                  <Button variant="secondary">Learn More</Button>
                </Link>
              </div>
              <div className="col-md-6">
                <h3 className="type-heading-h3">Developers</h3>
                <h3 className="type-heading-h3-secondary mb-3">
                  Flight is built to help developers implement working product
                  experiences faster. Using the component blueprints as a guide,
                  product teams and/or the Flight DS team will translate them to
                  framework-specific implementations over time.
                </h3>
                <Link to="/developers/product-teams">
                  <Button variant="secondary">Learn More</Button>
                </Link>
              </div>
            </div>
          </div>

          <div className="col-md-3">
            <h2 className="type-heading-h2-xl mb-3">
              Flight Design System Goals
            </h2>
            <Accordion>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  1. Promote Consistency
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    Establish voice, look and feel that allows teams to express
                    the ARC brand in a clear and consistent manner across our
                    digital experiences. Provide consistently usable
                    interactions that are familiar and cohesive from one digital
                    channel to the next.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                  2. Improve Collaboration
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>
                    Enable designers and developers to contribute, reuse, and
                    continuously improve design patterns to more effectively
                    create quality interfaces together.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="2">
                  3. Foster Design Culture
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="2">
                  <Card.Body>
                    Provide an artifact of ARC's design culture that
                    communicates the philosophy, vision, and vocabulary of our
                    design organization.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="3">
                  4. Build for the Future
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="3">
                  <Card.Body>
                    Build upon the system's future-friendly foundations with
                    strategies and tools that are modular, scalable, and
                    flexible to changes in design and technology.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </div>
        </div>
      </div>
    </div>

    <div className="homepage-card-container">
      <div className="container">
        <h2 className="type-heading-h2-xl text-center">Libraries & Guides</h2>
        <div className="homepage-card-grid hero-card-grid-4">
          <a
            target="_blank"
            href="https://sketch.cloud/s/ML42z"
            className="homepage-card homepage-card-1 homepage-card-theme-blue"
          >
            <div className="homepage-card-copy">
              Sketch
              <br />
              Libraries
            </div>
            <div className="homepage-card-meta">
              <img
                src="https://www2.arccorp.com/globalassets/fds/fds-sketch-icon.png"
                alt="Sketch"
              />
              <div className="homepage-card-link">Access Library</div>
            </div>
            <div className="homepage-card-icon"></div>
          </a>
          <a
            href="/component-blueprints/overview"
            className="homepage-card homepage-card-2 homepage-card-theme-teal"
          >
            <div className="homepage-card-copy">
              Flight
              <br />
              Components
              <br />
            </div>
            <div className="homepage-card-meta">
              <img
                src="https://www2.arccorp.com/globalassets/fds/fds-react-icon.png"
                alt="Sketch"
              />
              <div className="homepage-card-link">Get Components</div>
            </div>
            <div className="homepage-card-icon"></div>
          </a>
          <a
            target="_blank"
            href="https://www2.arccorp.com/styleguide"
            className="homepage-card homepage-card-2 homepage-card-theme-brown"
          >
            <div className="homepage-card-copy">
              ARC Brand
              <br />& Style Guide
            </div>
            <div className="homepage-card-meta">
              <img
                src="https://www2.arccorp.com/globalassets/fds/fds-arc-icon.png"
                alt="Sketch"
              />
              <div className="homepage-card-link">Access Components</div>
            </div>
            <div className="homepage-card-icon"></div>
          </a>
          <a
            href="/guidelines/iconography"
            className="homepage-card homepage-card-2  homepage-card-theme-grey"
          >
            <div className="homepage-card-copy">
              Icon
              <br />
              Library
            </div>
            <div className="homepage-card-meta">
              <img
                src="https://www2.arccorp.com/globalassets/fds/fds-sketch-icon.png"
                alt="Sketch"
              />
              <div className="homepage-card-link">Access Library</div>
            </div>

            <div className="homepage-card-icon"></div>
          </a>
        </div>
      </div>
    </div>

    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-lg-7">
            <p>&copy; 2019 Airlines Reporting Corporation</p>
            <p>
              SENSITIVE AND CONFIDENTIAL. Data presented or downloaded here may
              contain ARC-sensitive information. Disclosure to 3rd parties is
              prohibited without prior written consent of ARC.
            </p>
          </div>
          <div className="col-lg-5">
            <div className="footer-links">
              <a className="fds-link" href="#">
                Terms Of Use
              </a>
              <a className="fds-link" href="#">
                Privacy Policy
              </a>
              <a className="fds-link" href="#">
                Contact Us
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export const Getting_Started = () => (
  <Container className="mt-5">
    <Row>
      <Col>
        <h1 className="type-heading-h1-xl">Getting Started</h1>
        <p>
          The flight-dev-kit is a collection of SCSS styles for the Flight
          Design System design tokens and theming for Bootstrap 4.
        </p>

        <pre>
          <code>
            <div dangerouslySetInnerHTML={copyCodeBlock1()} />
          </code>
        </pre>
      </Col>
    </Row>
  </Container>
);

class Link extends React.Component {
  render() {
    return (
      <a className={this.props.className} href={this.props.to}>
        {this.props.children}
      </a>
    );
  }
}

Overview.story = {
  parameters: {
    paddings: { disabled: true }
  }
};
