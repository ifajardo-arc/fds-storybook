import React from "react";
import { action } from "@storybook/addon-actions";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";

export default {
  title: "Accordion",
  component: Accordion
};

export const Overview = () => (
  <div class="container">
    <h2 className="type-heading-h2">Accordion</h2>
    <p>
      The accordion component is used to display large amounts of content when
      space is limited. They are helpful to use when the underlying content is
      not important to see right away, and they work especially well on mobile
      views where additional vertical space is available.
    </p>

    <Accordion>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="0">
          Accordion 1
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit
            minus vero, cupiditate dolorem minima asperiores esse at! Odio
            facilis quidem nulla exercitationem doloribus et eaque ad earum
            minima, quis voluptates.
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="1">
          Accordion 2
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1">
          <Card.Body>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit
            minus vero, cupiditate dolorem minima asperiores esse at! Odio
            facilis quidem nulla exercitationem doloribus et eaque ad earum
            minima, quis voluptates.
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  </div>
);

export const Basic = () => (
  <div class="container">
    <Accordion>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="0">
          Accordion 1
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit
            minus vero, cupiditate dolorem minima asperiores esse at! Odio
            facilis quidem nulla exercitationem doloribus et eaque ad earum
            minima, quis voluptates.
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Accordion.Toggle as={Card.Header} eventKey="1">
          Accordion 2
        </Accordion.Toggle>
        <Accordion.Collapse eventKey="1">
          <Card.Body>
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit
            minus vero, cupiditate dolorem minima asperiores esse at! Odio
            facilis quidem nulla exercitationem doloribus et eaque ad earum
            minima, quis voluptates.
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  </div>
);
