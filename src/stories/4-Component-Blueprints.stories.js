import React from "react";


export default {
  title: "Component Blueprints",
  parameters: {
    options: { showPanel: false }
  }
};

export const Overview = () => <div></div>;

export const Getting_Started = () => <div></div>;

class Link extends React.Component {
  render() {
    return (
      <a className={this.props.className} href={this.props.to}>
        {this.props.children}
      </a>
    );
  }
}

